import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import { Pagination, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import articlesGroupedByEvents from "./data/grouped_by_event_articles_sample.json";
import ArticleList from "./components/ArticleList";
import DetailView from "./components/DetailView";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  color: theme.palette.text.secondary,
  height: "100%",
  boxSizing: "border-box",
}));

function App() {
  const [page, setPage] = useState(1);
  const [selectedArticle, setSelectedArticle] = useState(0);
  const pageCount = Object.keys(articlesGroupedByEvents)?.length;
  const selectedEventId = Object.keys(articlesGroupedByEvents)[page - 1];
  useEffect(() => {
    setSelectedArticle(0);
  }, [page]);
  return (
    <Box style={{ backgroundColor: "#eee", minHeight: 0 }} height="100%">
      <Box padding={2} height="calc(100% - 40px)">
        <Box
          display="grid"
          gridTemplateColumns="repeat(3, minmax(0, 1fr))"
          gridTemplateRows="5% minmax(0, 1fr)"
          style={{ height: "100%" }}
          gap={2}
          gridTemplateAreas={`
          "eventList eventList eventList"
          "articleList detailView detailView"
          `}
        >
          <Box gridArea="eventList" minHeight={0}>
            <Item>
              <Box display="flex" justifyContent="center" alignContent="center">
                <Box style={{ display: "flex", alignItems: "center" }}>
                  <Typography sx={{ marginRight: 2 }}>Events:</Typography>
                  <Pagination
                    count={pageCount}
                    color="primary"
                    variant="outlined"
                    size="large"
                    page={page}
                    onChange={(_event, value) => {
                      setPage(value);
                    }}
                  />
                </Box>
              </Box>
            </Item>
          </Box>
          <Box gridArea="articleList" minHeight={0}>
            <Item style={{ overflow: "auto" }}>
              <ArticleList
                list={articlesGroupedByEvents[selectedEventId]}
                selectedArticle={selectedArticle}
                setSelectedArticle={setSelectedArticle}
              />
            </Item>
          </Box>
          <Box gridArea="detailView" minHeight={0}>
            <Item style={{ overflow: "auto" }}>
              <DetailView
                article={
                  articlesGroupedByEvents[selectedEventId][selectedArticle]
                }
              />
            </Item>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}

export default App;
