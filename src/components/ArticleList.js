import {
  Divider,
  List,
  ListItemButton,
  ListItemText,
  Typography,
} from "@mui/material";

export default function ArticleList({
  list,
  selectedArticle,
  setSelectedArticle,
}) {
  return (
    <List>
      {list.map((element, index) => {
        return (
          <>
            <Typography>{element["uri"]}</Typography>
            <ListItemButton
              selected={selectedArticle === index}
              onClick={(_event) => setSelectedArticle(index)}
              key={element["uri"]}
            >
              <ListItemText
                primary={element["title"]}
                secondary={element["source"]["title"]}
              />
            </ListItemButton>
            <Divider key={`Divider-${element["uri"]}`} />
          </>
        );
      })}
    </List>
  );
}
