import {
  Avatar,
  Chip,
  Link,
  List,
  ListItem,
  ListItemText,
  Paper,
  Typography,
} from "@mui/material";
import { useState } from "react";
import PlaceIcon from "@mui/icons-material/Place";
import PersonIcon from "@mui/icons-material/Person";
import OrganizationIcon from "@mui/icons-material/Domain";

const getProperIcon = (type) => {
  switch (type) {
    case "loc":
      return <PlaceIcon />;
    case "org":
      return <OrganizationIcon />;
    case "person":
      return <PersonIcon />;
    default:
      return null;
  }
};

export default function DetailView({ article }) {
  const [expand, setExpand] = useState(false);
  return (
    <>
      <Paper variant="outlined" sx={{ padding: 1, marginBottom: 1 }}>
        <Typography variant="h5" paddingY={1}>
          {article?.title}
        </Typography>
        <Typography variant="caption">
          {expand ? article?.body : article?.body.substring(0, 1000)}
          {
            <Link onClick={() => setExpand(!expand)} href="#">
              {expand ? "Show Less" : "... Show More"}
            </Link>
          }
        </Typography>
      </Paper>
      <Paper variant="outlined" sx={{ padding: 1, marginY: 1 }}>
        <Typography>{article?.concepts?.length}</Typography>
        {article?.concepts
          // ?.filter((item) => item?.score > 2)
          ?.map((item) => {
            return (
              <Chip
                sx={{
                  margin: 0.5,
                  // opacity: `${Math.min(item?.score * 20 + 10, 100)}%`,
                }}
                label={item.label.eng}
                clickable
                component="a"
                key={item.label.eng}
                href={item.uri}
                {...(item.type === "wiki"
                  ? {
                      avatar: (
                        <Avatar src="https://upload.wikimedia.org/wikipedia/commons/2/2c/Tango_style_Wikipedia_Icon.svg" />
                      ),
                    }
                  : { icon: getProperIcon(item?.type) })}
              />
            );
          })}
      </Paper>
      <Paper variant="outlined" sx={{ padding: 1, marginY: 1 }}>
        <List>
          {article?.categories?.map((item) => {
            return (
              <ListItem>
                <ListItemText
                  primary={
                    item["label"].split("/")[
                      item["label"].split("/").length - 1
                    ]
                  }
                  secondary={item["label"]}
                />
              </ListItem>
            );
          })}
        </List>
      </Paper>
    </>
  );
}
